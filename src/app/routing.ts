import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CallbackComponent } from '@app/shared';
import { AppComponent } from './app.component';

import { LoggedInGuard } from '@app/core';

const routes: Routes = [
    {
        path: 'callback',
        component: CallbackComponent
    },
    {
        path: 'home',
        loadChildren: './main/main.module#MainModule',
        canActivate: [LoggedInGuard]
    }  
];
  
export const routing: ModuleWithProviders = RouterModule.forChild(routes);