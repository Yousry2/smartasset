import { Component } from '@angular/core';

import { AuthService } from './core/auth/auth.service';
import { LoggerService } from './core/logger/logger.service';
import { LogMessage } from './core/logger/log-message';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(public auth: AuthService, private loggerService: LoggerService) {
    loggerService.info(new LogMessage("Log info"));
    auth.handleAuthentication();    
  }

  login() {
    this.auth.login();
  }
}
