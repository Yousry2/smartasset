import { NgModule } from '@angular/core';
import {SplitButtonModule } from 'primeng/primeng';

@NgModule({
  imports: [SplitButtonModule],
  exports: [SplitButtonModule]
})
export class CustomNgPrimeModule { }
