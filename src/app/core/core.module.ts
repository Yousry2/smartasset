import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AuthService } from './auth/auth.service';
import { Auth0Service } from './auth/auth0/auth0.service';
import { ConsoleLoggerService } from './logger/console-logger/console-logger.service';
import { LoggerService } from './logger/logger.service';
import { LoggedInGuard } from './guards/logged-in.guard';

@NgModule({
  declarations: [
  ],
  imports: [
    RouterModule.forRoot([])
  ],
  providers: [
    LoggedInGuard,
    { provide: AuthService, useClass: Auth0Service },
    { provide: LoggerService, useClass: ConsoleLoggerService }
  ]
})
export class CoreModule { }
