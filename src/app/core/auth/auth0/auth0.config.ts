import { Injectable } from '@angular/core';


@Injectable()
export class Auth0Config {

  clientID = 'r0S6inu-ddb1Zxn7PMS0g550DyniY4mX';
  domain = 'thoughtdesign.au.auth0.com';
  responseType = 'token id_token';
  audience = 'https://thoughtdesign.au.auth0.com/userinfo';
  redirectUri = 'http://localhost:4200/callback';
  scope = 'openid';
}
